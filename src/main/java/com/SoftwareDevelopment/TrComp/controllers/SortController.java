package com.SoftwareDevelopment.TrComp.controllers;


import com.SoftwareDevelopment.TrComp.models.Sort;
import com.SoftwareDevelopment.TrComp.models.Type;
import com.SoftwareDevelopment.TrComp.services.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/sort")
public class SortController {
    public SortController(SortService sortService, TypeService typeService) {
        this.sortService = sortService;
        this.typeService = typeService;
    }

    private SortService sortService;
    private TypeService typeService;


    @GetMapping("/list")
    public String customerSort(Pageable page, Model model) {

        model.addAttribute("sort", sortService.findAll());

        return "sort/sortsList";
    }

    @GetMapping("/add")
    public String sort (Model model){

        Sort sort = new Sort();

        model.addAttribute("sort", sort);

        return "sort/update-form";
    }

    @GetMapping("/delete")
    public String sortDelete(@RequestParam("id") int id) {
        sortService.deleteById(id);

        return "redirect:/sort/list";
    }

    @PostMapping("/save")
    public String sortSave (@ModelAttribute("sort") Sort sort) {
        sortService.save(sort);
        return "redirect:/sort/list";
    }

    @GetMapping("/update")
    public String sortUpdate (@RequestParam("id") Integer id, Model model) {

        model.addAttribute("sort", sortService.findById(id));

/*        model.addAttribute("privilegeList", privilegeService.findAll());

        model.addAttribute("genderList", genderService.findAll());*/

        return "sort/update-form";
    }

    @GetMapping("/types")
    public String sortTypes (@RequestParam("id") Integer id, Model model) {

        model.addAttribute("sort", sortService.findById(id));

        return "sort/types";
    }

    @GetMapping("/selectType")
    public String selectSorts(@RequestParam("id") int id, Model model) {

        model.addAttribute("sort", sortService.findById(id));
        model.addAttribute("typeList", typeService.findAll());
        return "sort/ADDtype";
    }

    @GetMapping("/addType")
    public String addType(@RequestParam int sortId, @RequestParam int typeId, Model model) {

        Sort sort = sortService.findById(sortId);
        Type type = typeService.findById(typeId);
        type.getSorts().add(sort);
        sort.getTypes().add(type);
        typeService.save(type);
        sortService.save(sort);
        return "redirect:/sort/types?id="+sortId;
    }

    @GetMapping("/delatetype")
    public String sortDeleteType(@RequestParam("id_sort") int id_sort,@RequestParam("id_type") int id_type) {
        Sort sort = sortService.findById(id_sort);
        for(Type s : sort.getTypes()){
            if(s.getId()==id_type){
                sort.getTypes().remove(s);
                s.setSorts(null);
                sortService.save(sort);
                typeService.save(s);
                break;
            }
        }

        return "redirect:/sort/list";
    }

}

