package com.SoftwareDevelopment.TrComp.controllers;


import com.SoftwareDevelopment.TrComp.models.Plaofcreat;
import com.SoftwareDevelopment.TrComp.models.Sort;
import com.SoftwareDevelopment.TrComp.services.PlaofcreateService;
import com.SoftwareDevelopment.TrComp.services.SortService;
import com.SoftwareDevelopment.TrComp.services.TypeService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.SoftwareDevelopment.TrComp.models.Type;

@Controller
@RequestMapping("/type")
public class TypeController {
    public TypeController(TypeService typeService, PlaofcreateService plaofcreateService, SortService  sortService) {
        this.plaofcreateService = plaofcreateService;
        this.typeService = typeService;
        this.sortService = sortService;
    }

    private TypeService typeService;
    private PlaofcreateService plaofcreateService;
    private SortService sortService;


    @GetMapping("/plaofcreat")
    public String typePlaofcreate(@RequestParam("id") int id, Model model) {
        Type type = typeService.findById(id);
        model.addAttribute("type", typeService.findById(id));
        return "type/plaofcreate";
    }

    @GetMapping("/addPlaofcreat")
    public String customerPlaofcreat(@RequestParam int typeId, @RequestParam int plaofcreatId, Model model) {

        Type type = typeService.findById(typeId);
        Plaofcreat plaofcreat = plaofcreateService.findById(plaofcreatId);
        plaofcreat.setType(type);
        type.getPlaofcreates().add(plaofcreat);
        plaofcreateService.save(plaofcreat);
        typeService.save(type);
        return "redirect:/type/plaofcreat?id="+typeId;
    }

    @GetMapping("/list")
    public String customerType(Pageable page, Model model) {

        model.addAttribute("type", typeService.findAll());

        return "type/typeList";
    }


    @GetMapping("/delete")
    public String typeDelete(@RequestParam("id") int id) {
        typeService.deleteById(id);

        return "redirect:/type/list";
    }

    @GetMapping("/add")
    public String type(Model model) {

        Type type = new Type();

        model.addAttribute("type", type);

        return "type/update-form";
    }

    @PostMapping("/save")
    public String typeSave(@ModelAttribute("type") Type type) {
        typeService.save(type);
        return "redirect:/type/list";
    }


    @GetMapping("/update")
    public String typeUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("type", typeService.findById(id));
        return "type/update-form";
    }

    @GetMapping("/selectPlaofcreat")
    public String addPlaofcreat(@RequestParam("id") int id, Model model) {

        model.addAttribute("type", typeService.findById(id));
        model.addAttribute("plaofcreatList", plaofcreateService.findAll());
        return "type/ADDplaofcreate";
    }

    @GetMapping("/deleteplace")
    public String typeDeletePlace(@RequestParam("id_type") int id_type,@RequestParam("id_place") int id_place) {
        Type type = typeService.findById(id_type);
        for(Plaofcreat s : type.getPlaofcreates()){
            if(s.getId()==id_place){
                type.getPlaofcreates().remove(s);
                s.setType(null);
                typeService.save(type);
                plaofcreateService.save(s);
                break;
            }
        }
        return "redirect:/type/list";
    }

    @GetMapping("/showsorts")
    public String showSorts(@RequestParam("id") int id, Model model) {

        model.addAttribute("type", typeService.findById(id));
        model.addAttribute("sortList", sortService.findAll());
        return "type/showSort";
    }

    @GetMapping("/delatesort")
    public String typeDeleteSort(@RequestParam("id_type") int id_type,@RequestParam("id_sort") int id_sort) {
        Type type = typeService.findById(id_type);
        for(Sort s : type.getSorts()){
            if(s.getId()==id_sort){
                type.getSorts().remove(s);
                s.setTypes(null);
                typeService.save(type);
                sortService.save(s);
                break;
            }
        }

        return "redirect:/type/list";
    }

    @GetMapping("/selectSort")
    public String selectSorts(@RequestParam("id") int id, Model model) {

        model.addAttribute("type", typeService.findById(id));
        model.addAttribute("sortList", sortService.findAll());
        return "type/ADDsort";
    }

    @GetMapping("/addSort")
    public String addSort(@RequestParam int typeId, @RequestParam int sortId, Model model) {

        Type type = typeService.findById(typeId);
        Sort sort = sortService.findById(sortId);
        sort.getTypes().add(type);
        type.getSorts().add(sort);
        sortService.save(sort);
        typeService.save(type);
        return "redirect:/type/showsorts?id="+typeId;
    }

}
