package com.SoftwareDevelopment.TrComp.controllers;


import com.SoftwareDevelopment.TrComp.models.Plaofcreat;
import com.SoftwareDevelopment.TrComp.services.PlaofcreateService;
import com.SoftwareDevelopment.TrComp.services.TypeService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/plaofcreat")
public class PlaofcreatContreller {

    public PlaofcreatContreller (TypeService typeService, PlaofcreateService plaofcreateService){
        this.typeService = typeService;
        this.plaofcreateService = plaofcreateService;
    }

    private TypeService typeService;
    private PlaofcreateService plaofcreateService;

    @GetMapping("/list")
    public String customerPlaofcreate(Pageable page, Model model) {

        model.addAttribute("plaofcreate", plaofcreateService.findAll());  // (содержимое БД)

        return "plaofcreate/plaofcreateList";
    }

    @GetMapping("/update")
    public String plaofcreateUPD (@RequestParam("id") Integer id, Model model) {

        model.addAttribute("plaofcreate", plaofcreateService.findById(id));

        return "plaofcreate/update-form";
    }

    @GetMapping("/add")
    public String plaofcreateADD(Model model) {

        Plaofcreat plaocreate = new Plaofcreat();

        model.addAttribute("plaofcreate", plaocreate); //Добавляем в модель (представление)  параметр / (имя параметра,параметр)

        return "plaofcreate/update-form";
    }

    @GetMapping("/delete")
    public String plaofcreateDel(@RequestParam("id") int id) {
        plaofcreateService.deleteById(id);

        return "redirect:/plaofcreat/list";
    }

    @PostMapping("/save")
    public String plaofcreateSave(@ModelAttribute("plaofcreate") Plaofcreat plaofcreat) {
        plaofcreateService.save(plaofcreat);
        return "redirect:/plaofcreat/list";
    }
}
