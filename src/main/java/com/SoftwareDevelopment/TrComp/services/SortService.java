package com.SoftwareDevelopment.TrComp.services;


import com.SoftwareDevelopment.TrComp.models.Sort;
import com.SoftwareDevelopment.TrComp.repositories.SortRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SortService{
    @Autowired //создание объекта определенного класса
    SortRepository repository;
    public Sort findById(Integer id) {
        Optional<Sort> result = repository.findById(id);
        Sort n = null;
        if (result.isPresent()) {
            n = result.get();
        }
        else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }
    public Iterable<Sort> findAll() {
        return repository.findAll();
    }

    public Iterable<Sort> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Iterable<Sort> findAll(org.springframework.data.domain.Sort sort) {
        return repository.findAll(sort);
    }

    public void save(Sort sort) {
        repository.save(sort);
    }

    public void deleteById(Integer id){
        repository.deleteById(id);
    }
}

