package com.SoftwareDevelopment.TrComp.services;


import com.SoftwareDevelopment.TrComp.models.Type;
import com.SoftwareDevelopment.TrComp.repositories.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TypeService {
    @Autowired
    TypeRepository repository;
    public Type findById(Integer id) {
        Optional<Type> result = repository.findById(id);
        Type n = null;
        if (result.isPresent()) {
            n = result.get();
        }
        else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Type> findAll() {
        return repository.findAll();
    }

    public Iterable<Type> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Iterable<Type> findAll(org.springframework.data.domain.Sort sort) {
        return repository.findAll(sort);
    }

    public void save(Type type) {
        repository.save(type);
    }

    public void deleteById(Integer id){
        repository.deleteById(id);
    }
}
