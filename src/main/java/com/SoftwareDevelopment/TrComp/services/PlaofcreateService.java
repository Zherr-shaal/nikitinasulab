package com.SoftwareDevelopment.TrComp.services;


import com.SoftwareDevelopment.TrComp.models.Plaofcreat;
import com.SoftwareDevelopment.TrComp.repositories.PlaofcreateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlaofcreateService {
    @Autowired
    PlaofcreateRepository repository;
    public Plaofcreat findById(Integer id) {
        Optional<Plaofcreat> result = repository.findById(id);
        Plaofcreat n = null;
        if (result.isPresent()) {
            n = result.get();
        }
        else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Plaofcreat> findAll() {
        return repository.findAll();
    }

    public Iterable<Plaofcreat> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Iterable<Plaofcreat> findAll(org.springframework.data.domain.Sort sort) {
        return repository.findAll(sort);
    }

    public void save(Plaofcreat plaofcreate) {
        repository.save(plaofcreate);
    }

    public void deleteById(Integer id){
        repository.deleteById(id);
    }

}

//надстрой над репозом, служат для того, чтобы прятать от пользователя не нужный для интерфейса функционал