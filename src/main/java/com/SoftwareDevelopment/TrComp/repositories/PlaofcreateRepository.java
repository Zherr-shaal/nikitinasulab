package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Plaofcreat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaofcreateRepository extends
   JpaRepository <Plaofcreat, Integer> { };
