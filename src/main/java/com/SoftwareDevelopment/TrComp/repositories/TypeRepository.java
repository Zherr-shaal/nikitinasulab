package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Type;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeRepository extends
        JpaRepository<Type,Integer> { };
