package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SortRepository extends
        JpaRepository<Sort, Integer> { };
 // Наследуют JPA , который позволяет сохранят объект  в бд. *(позволяет сохранять объекты в БД)*

/*
        Аннотация Repository расширяет аннотацию Component,
        что позволяет имплементированный класс делать бином и соответственно аутовайрить.

        Расширение интерфейса JpaRepository позволяет производить нужные CRUD операции без дополнительного описания
        */
