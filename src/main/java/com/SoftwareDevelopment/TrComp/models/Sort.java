package com.SoftwareDevelopment.TrComp.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Sort {
    @Id //Id @Column(name=«id таблицы в базе»);
    @Column(name = "id_sort")  //Столбец БД
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**GenerationType.IDENTITY — самый простой способ конфигурирования генератора.
     * Он опирается на auto-increment колонку в таблице.
     * Следовательно, чтобы получить id при persist-е нам нужно сделать insert.
     * Именно поэтому он исключает возможность отложенного persist-а и следовательно batching-а.
     */

    /**аннотациями @GeneratedValue и @SequenceGenerator мы определяем стратегию генерации уникального идентификатора,
 *  в данном случае мы говорим, что при сохранении информации в базу данных,
 *  мы берем число из strategy с названием «id_sort»;**/

    @Column(name = "name_sort")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "sorts",
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    private Set<Type> types;


/******* fetch  *******
//    С подходом ленивой инициализации  будет инициализироваться только тогда,
//    когда он явно вызывается с помощью getter или другого метода


//    Но при EAGER подходе  он будет инициализирован немедленно в первой строке
// fetch - определяет тип связи,  mappedBy - поле связанного класса , cascade - настройка каскадного заимодействия (для обеспечения целостности данных)
 *******/

    /******* mappedBy  *******
     //имя поля другого класса, указываем кем маппится
     *******/

    @PreRemove  //очистка листов связей, для прохода каскадного обновления
    private void preRemove() {
        for (Type s : types) {
            s.getSorts().remove(this);
        }
    }

    /******* PreRemove  *******
     //используется для указания метода обратного вызова, который срабатывает до удаления объекта.
     *******/


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public void setTypes(Set<Type> types) {
        this.types = types;
    }
}
