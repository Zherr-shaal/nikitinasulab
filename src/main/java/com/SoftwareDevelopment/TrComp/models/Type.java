package com.SoftwareDevelopment.TrComp.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Type {
    @Id
    @Column(name = "id_type")  //Столбец БД
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_type")
    private String name;

    @OneToMany(fetch = FetchType.LAZY ,mappedBy = "type",  //указываем, что табл тайп подтягивает в лист данные о м.создания
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    private List<Plaofcreat> plaofcreates;


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    @JoinTable(name = "srm",
            joinColumns = { @JoinColumn(name = "type_fk", referencedColumnName="id_type")},
            inverseJoinColumns = { @JoinColumn(name = "sort_fk",referencedColumnName="id_sort")})
    private List<Sort> sorts;

    /** JoinColumn предназначен для того,
     * чтобы указать другой столбец в качестве столбца идентификатора по умолчанию другой таблицы **/

    public List<Sort> getSorts() {
        return sorts;
    }

    public void setSorts(List<Sort> sorts) {
        this.sorts = sorts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Plaofcreat> getPlaofcreates() {
        return plaofcreates;
    }

    public void setPlaofcreates(List<Plaofcreat> plaofcreates) {
        this.plaofcreates = plaofcreates;
    }

}

